#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "RockZehh"
#define PLUGIN_VERSION "1.0.0b"

#include <sourcemod>
#include <sdktools>
#include <morecolors>

#pragma newdecls required

bool g_bDisguise[MAXPLAYERS + 1] = false;

Handle g_hDisguise;

int g_iDisguise[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name = "Disguise",
	author = PLUGIN_AUTHOR,
	description = "Run around as a prop.",
	version = PLUGIN_VERSION,
	url = "https://bitbucket.org/rockzehh/miscellaneous-plugins/"
};

public void OnPluginStart()
{
	RegAdminCmd("sm_disguise", Command_Disguise, ADMFLAG_BAN, "Disguise the admin.");
	
	//Removes the cheat flag from 'thirdperson':
	int iFlags = GetCommandFlags("thirdperson");
	
	if(iFlags != INVALID_FCVAR_FLAGS)
	{
		SetCommandFlags("thirdperson", iFlags & ~FCVAR_CHEAT);
	}
}

public void OnClientPutInServer(int iClient)
{
	g_bDisguise[iClient] = false;
	
	g_iDisguise[iClient] = -1;
}

public void OnClientDisconnect(int iClient)
{
	g_bDisguise[iClient] = false;
	
	AcceptEntityInput(g_iDisguise[iClient], "kill");

	g_iDisguise[iClient] = -1;
}

public void OnMapStart()
{
	g_hDisguise = CreateTimer(0.1, Timer_Disguise, _, TIMER_REPEAT);
}

public void OnMapEnd()
{
	CloseHandle(g_hDisguise);
}

//Commands:
public Action Command_Disguise(int iClient, int iArgs)
{
	Flare_PreformDisguise(iClient);
	
	return Plugin_Handled;
}

//Stocks:
public void Flare_PreformDisguise(int iClient)
{
	if(g_bDisguise[iClient])
	{
		AcceptEntityInput(g_iDisguise[iClient], "kill");
		
		g_iDisguise[iClient] = -1;
		
		SetClientViewEntity(iClient, iClient);
		
		SetEntProp(iClient, view_as<PropType>(Prop_Data), "m_CollisionGroup", 0);
		
		SetEntProp(iClient, view_as<PropType>(Prop_Data), "m_takedamage", 2, 1);
		
		SetEntityMoveType(iClient, MOVETYPE_WALK);
		
		ClientCommand(iClient, "thirdperson");
		
		g_bDisguise[iClient] = false;
		
		CReplyToCommand(iClient, "{blue}Disguise{default}: {red}Disabled{default} Disguise Mode");
	}else{
		char sModel[128];
		float fAngles[3], fOrigin[3];
		
		ClientCommand(iClient, "thirdperson");
		
		GetClientAbsAngles(iClient, fAngles);
		GetClientAbsOrigin(iClient, fOrigin);
		
		Format(sModel, sizeof(sModel), "models/props_junk/watermelon01.mdl");
		
		g_iDisguise[iClient] = CreateEntityByName("prop_physics_override");
		
		PrecacheModel(sModel);
		
		DispatchKeyValue(g_iDisguise[iClient], "model", sModel);
		
		DispatchSpawn(g_iDisguise[iClient]);
		
		SetEntProp(iClient, view_as<PropType>(Prop_Data), "m_CollisionGroup", 1);
		
		SetEntProp(iClient, view_as<PropType>(Prop_Data), "m_takedamage", 0, 1);
		
		fOrigin[2] += 64;
		
		TeleportEntity(g_iDisguise[iClient], fOrigin, fAngles, NULL_VECTOR);
		
		SetClientViewEntity(iClient, g_iDisguise[iClient]);
		
		SetEntityMoveType(iClient, MOVETYPE_NONE);
		
		g_bDisguise[iClient] = true;
		
		ReplyToCommand(iClient, "{blue}Disguise{default}: {green}Enabled{default} Disguise Mode");
	}
}

//Timers:
public Action Timer_Disguise(Handle hTimer)
{
	float fAngles[3], fVelocity[3];
	int iButtons, iFlags;
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(g_bDisguise[i])
		{
			iButtons = GetClientButtons(i);
			iFlags = GetEntityFlags(i);
			
			if(iButtons & IN_FORWARD && iFlags & FL_ONGROUND)
			{
				GetClientAbsAngles(i, fAngles);
				
				fVelocity[0] = Cosine(DegToRad(fAngles[1])) * 700;
				fVelocity[1] = Sine(DegToRad(fAngles[1])) * 700;
				fVelocity[2] = Sine(DegToRad(fAngles[0])) * -1680;
				
				TeleportEntity(g_iDisguise[i], NULL_VECTOR, fAngles, fVelocity);
			}else if(iButtons & IN_BACK && iFlags & FL_ONGROUND)
			{
				GetClientAbsAngles(i, fAngles);
				
				fVelocity[0] = Cosine(DegToRad(fAngles[1])) * -700;
				fVelocity[1] = Sine(DegToRad(fAngles[1])) * -700;
				fVelocity[2] = Sine(DegToRad(fAngles[0])) * 1680;
				
				TeleportEntity(g_iDisguise[i], NULL_VECTOR, fAngles, fVelocity);
			}
		}
	}
}
